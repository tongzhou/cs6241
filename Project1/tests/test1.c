#include "stdlib.h"
#include "stdbool.h"


void foo() {
  int x, y, w, v;
  bool cd1, cd2, cd3, cd4;
  w = 1;
  y = 2;

  if (cd1) {
    
  }
  else {
    v = 5;
    x = y + 1;
  }

  cd2 = v == 5;
  
  if (cd2) {
    
  }
  else {
    printf("unreachable\n");
  }
}

void switch1(int v) {
  switch (v) {
  case 1:
    printf("v is 1\n");
    break;
  case 2:
    printf("v is 2\n");
    break;
  }
}
