# Build
Place the directory in `llvm/lib/tools/` and add the directory into `llvm/lib/tools/CMakeLists.txt`.
Then do a standard LLVM build and this pass will be built together into a `llvm/build/lib/LLVM_CS6241_HW1.so`

# Run
Run project part 1:
```bash
$ opt -load $(LLVM_BUILD_DIR)/lib/LLVM_CS6241_HW1.so -project1 xxx.ll
```

