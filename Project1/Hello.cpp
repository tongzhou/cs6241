//===- Hello.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/CFG.h"
#include "llvm/Analysis/CFG.h"
#include "llvm/ADT/SCCIterator.h"
#include "llvm/Pass.h"
#include "llvm/PassSupport.h"
#include "llvm/Support/raw_ostream.h"
//#include <llvm/IR/Dominators.h>
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Support/Format.h"
#include "llvm/Analysis/LoopAnalysisManager.h"

#include <set>
#include <queue>
#include <limits>
#include <cstddef>
#include <chrono>

using namespace llvm;




class MyTimer
{
public:
  MyTimer() : beg_(clock_::now()) {}
  void reset() { beg_ = clock_::now(); }
  double elapsed() const {
    return std::chrono::duration_cast<second_>
      (clock_::now() - beg_).count(); }

private:
  typedef std::chrono::high_resolution_clock clock_;
  typedef std::chrono::duration<double, std::milli > second_;
  std::chrono::time_point<clock_> beg_;
};



namespace {

class ValueRange {
public:
  double low;
  double high;
  
  ValueRange(double low, double high): low(low), high(high) {}

  double max() {
    return std::numeric_limits<double>::max();
  }

  double min() {
    return std::numeric_limits<double>::min();
  }
};

struct Project1 : public ModulePass {
  static char ID;
  bool _verbose = true;
  bool _printBounds = true;
  std::map<BasicBlock*, std::map<std::string, ValueRange*>> _blockValueMap;
public:
    
  Project1() : ModulePass(ID) {
      
  }

  bool runOnModule(Module &M) override {
    if (_verbose) {
      outs() << M.getName() << "\n";
    }

    for (auto& F: M) {
      doFunc(F);
    }

    return false;
  }

  void doFunc(Function& F) {
    if (_verbose) {
      outs() << F.getName() << "\n";
    }

    for (auto& B: F) {
      doIntraBlockAssertions(B);
    }
  }
    
  /*!
    Four types of assertions:
    1. constant assignment.
    2. prior conditional branch.
    3. type conversion. e.g. unsigned to signed
    4. pointer dereferencing
      
  */
  void doIntraBlockAssertions(BasicBlock& B) {
    _blockValueMap[&B] = std::map<std::string, ValueRange*>();

    /* Assertion: prior conditional branch. */
    doPriorBranchAssertion(B);
    
    for (auto& I: B) {
      
      /* Assertion constant assignment. */
      if (StoreInst* si = dyn_cast<StoreInst>(&I)) {
        outs() << I << "\n";
        si->getValueOperand()->dump();
        Value* value = si->getValueOperand();
        Value* pointer = si->getPointerOperand();
        if (ConstantInt* ci = dyn_cast<ConstantInt>(value)) {      
          value->dump();
          outs() << si->getPointerOperand()->getName() << '\n';
          updateValueLowerBound(&B, pointer, (double)ci->getSExtValue());
          updateValueHigherBound(&B, pointer, (double)ci->getSExtValue());
        }
        else if (ConstantFP* cf = dyn_cast<ConstantFP>(value)) {
          // TODO
        }
          
      }

      
    }
  }

  void doPriorBranchAssertion(BasicBlock& B) {
    /* Assertion: prior conditional branch. */
    BasicBlock* pred = B.getSinglePredecessor();
    Instruction* term = pred->getTerminator();

    /* Get the conditional associated with B and make it an assertion for B */
    
  }

  void updateValueLowerBound(BasicBlock* B, Value* var, double low) {
    if (_printBounds) {
      outs() << "set " << var->getName()
             << " >= " << low
             << " in block " << B->getName() << '\n';
    }
  }

  void updateValueHigherBound(BasicBlock* B, Value* var, double high) {
    if (_printBounds) {
            outs() << "set " << var->getName()
             << " <= " << high
             << " in block " << B->getName() << '\n';
    }

  }

  void doBlock(BasicBlock& B) {
      
  }

};
}

char Project1::ID = 0;
static RegisterPass<Project1> project1pass("project1", "Pass for CS 6241 project 1");
