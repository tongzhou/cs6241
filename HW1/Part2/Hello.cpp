//===- Hello.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/CFG.h"
#include "llvm/Analysis/CFG.h"
#include "llvm/ADT/SCCIterator.h"
#include "llvm/Pass.h"
#include "llvm/PassSupport.h"
#include "llvm/Support/raw_ostream.h"
//#include <llvm/IR/Dominators.h>
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Support/Format.h"
#include "llvm/Analysis/LoopAnalysisManager.h"

#include <set>
#include <queue>
using namespace llvm;


#include <chrono>

class MyTimer
{
public:
  MyTimer() : beg_(clock_::now()) {}
  void reset() { beg_ = clock_::now(); }
  double elapsed() const {
    return std::chrono::duration_cast<second_>
        (clock_::now() - beg_).count(); }

private:
  typedef std::chrono::high_resolution_clock clock_;
  typedef std::chrono::duration<double, std::milli > second_;
  std::chrono::time_point<clock_> beg_;
};



//#define DEBUG_TYPE "hello"

//STATISTIC(HelloCounter, "Counts number of functions greeted");

namespace {
  struct HW1Part2 : public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
  public:
    size_t _funcNum = 0;

    size_t _minBBNum = 10000000000;
    size_t _maxBBNum = 0;
    size_t _totalBBNum = 0;

    size_t _minEdgeNum = 10000000000;
    size_t _maxEdgeNum = 0;
    size_t _totalEdgeNum = 0;

    size_t _minLoopNum = 10000000000;
    size_t _maxLoopNum = 0;
    size_t _totalLoopNum = 0;

    size_t _minLoopBBNum = 10000000000;
    size_t _maxLoopBBNum = 0;
    size_t _totalLoopBBNum = 0;

    size_t _totalDomNum = 0;

    bool _isFirst = true;
    bool _verbose = false;


    HW1Part2() : FunctionPass(ID) {
      //initializeLoopInfoWrapperPassPass(*PassRegistry::getPassRegistry());
    }

    bool doInitialization(Module& M) override {

    }

    void initSCCGroup() {

    }

    bool doFinalization(Module &M) override {
      reportP1();
      reportP2();
      reportP34();
      reportP5();
      return false;
    }

    bool runOnFunction(Function &F) override {
      if (_verbose) {
        outs() << F.getName() << "\n";
      }

      doP1(F);
      doP2(F);
      doP3_4(F);
      doP5(F);

      return false;
    }

    // P1: Average, maximum and minimum number of basic blocks inside functions
    void doP1(Function& F) {
      _funcNum++;
      size_t bbNum = F.getBasicBlockList().size();
      _totalBBNum += bbNum;

      if (bbNum > _maxBBNum) {
        _maxBBNum = bbNum;
      }

      if (bbNum < _minBBNum) {
        _minBBNum = bbNum;
      }

      if (_verbose) {
        outs() << "BB num: " << bbNum << "\n";
      }
    }

    void reportP1() {
      outs() << "average # of bb: " << _totalBBNum / (float)_funcNum << "\n";
      outs() << "maximum # of bb: " << _maxBBNum << "\n";
      outs() << "minimum # of bb: " << _minBBNum << "\n";
    }

    // P2: Average, maximum and minimum number of CFG edges inside functions.
    void doP2(Function& F) {
      size_t edgeCount = 0;
      BasicBlock& entry = F.getEntryBlock();
      std::queue<BasicBlock*> workset;
      std::set<BasicBlock*> visited;
      workset.push(&entry);
      visited.insert(&entry);

      while (!workset.empty()) {
        BasicBlock* B = workset.front();
        workset.pop();

        for (BasicBlock* suc: successors(B)) {
          edgeCount++;
          if (visited.find(suc) == visited.end()) {
            workset.push(suc);
            visited.insert(suc);
          }
        }

      }

      if (edgeCount > _maxEdgeNum) {
        _maxEdgeNum = edgeCount;
      }

      if (edgeCount < _minEdgeNum) {
        _minEdgeNum = edgeCount;
      }

      _totalEdgeNum += edgeCount;

      if (_verbose) {
        outs() << "edge num: " << edgeCount << "\n";
      }
    }

    void reportP2() {
      outs() << "average number of cfg edge: " << _totalEdgeNum / (float)_funcNum << "\n";
      outs() << "maximum number of cfg edge: " << _maxEdgeNum << "\n";
      outs() << "minimum number of cfg edge: " << _minEdgeNum << "\n";
    }

    // P3: Average, maximum and minimum number of single entry loops inside functions
    // (count each loop based on a back edge).
    // P4: Average, maximum and minimum number of loop basic blocks inside functions
    void doP3_4(Function& F) {
      LoopInfoWrapperPass &wrapperPass = getAnalysis<LoopInfoWrapperPass>();
      LoopInfo *LI = &wrapperPass.getLoopInfo();

      std::set<Loop*> loops;
      for (Loop *L: *LI) {
        loops.insert(L);
        for (auto subloop: *L) {
          loops.insert(subloop);
        }
      }

      size_t loopNum = 0;
      size_t loopBBNum = 0;
      for (auto& L: loops) {
        loopNum++;
        loopBBNum += L->getBlocks().size();
      }

      if (loopNum > _maxLoopNum) _maxLoopNum = loopNum;
      if (loopNum < _minLoopNum) _minLoopNum = loopNum;
      _totalLoopNum += loopNum;

      if (loopNum) {
        if (loopBBNum > _maxLoopBBNum) _maxLoopBBNum = loopBBNum;
        if (loopBBNum < _minLoopBBNum) _minLoopBBNum = loopBBNum;
        _totalLoopBBNum += loopBBNum;
      }


      if (_verbose) {
        outs() << "loop num: " << loopNum << "\n";
        outs() << "loop BB num: " << loopBBNum << "\n";
      }
    }

    void reportP34() {
      outs() << "max # of loop s: " << _maxLoopNum << "\n";
       outs() << "min # of loop s: " << _minLoopNum << "\n";
      outs() << "ave # of loop s: " << _totalLoopNum / (float)_funcNum << "\n";
      outs() << "max # of loop BBs: " << _maxLoopBBNum << "\n";
      outs() << "min # of loop BBs: " << _minLoopBBNum << "\n";
      outs() << "ave # of loop BBs: " << _totalLoopBBNum / (float)_funcNum << "\n";
    }

    // Average number of dominators for a basic block across all functions.
    void doP5(Function& F) {
      DominatorTree *DT = &getAnalysis<DominatorTreeWrapperPass>().getDomTree();

      size_t totalDescCount = 0;
      for (const auto node : depth_first(DT)) {
        BasicBlock* parentBB = node->getBlock();
        SmallVector<BasicBlock *, 8> descendants;
        DT->getDescendants(parentBB, descendants);
        totalDescCount += descendants.size();
      }

      outs() << "dom num: " << totalDescCount << "\n";
      _totalDomNum += totalDescCount;

      /*
       * DT->viewGraph();
       */
    }

    void reportP5() {
      outs() << "average number of dominators: " << _totalDomNum / (float)_totalBBNum << "\n";
    }

    // Only need to add stuff here to make LoopInfoWrapperPass available for use
    void getAnalysisUsage(AnalysisUsage &AU) const override {
      AU.addRequired<LoopInfoWrapperPass>();
      AU.addRequired<DominatorTreeWrapperPass>();
      AU.setPreservesAll();
    }

  };
}

char HW1Part2::ID = 0;
static RegisterPass<HW1Part2> hw1part2pass("hw1-part2", "Pass for part 2 in HW1");


namespace {
struct HW1Part3 : public FunctionPass {
  static char ID; // Pass identification, replacement for typeid

  bool _verbose = true;

  double _totalTime = 0;
  double _totalTest = 0;
  double _totalDefNum = 0;

  HW1Part3() : FunctionPass(ID) {

  }

  bool runOnFunction(Function &F) override {
    if (_verbose) {
      outs() << F.getName() << "\n";
    }

    doP3_1(F);
    //doP3_2(F);
    //doP3_4(F);
    //reportP3_4();
    return false;
  }

  bool doFinalization(Module &M) override {
    //reportP3_4();
    return false;
  }


  // P1: The number of loops in all functions in the input C file
  // P2: The number of loops that are outermost loops (not nested in any other loops)
  void doP3_1(Function& F) {
    LoopInfoWrapperPass &wrapperPass = getAnalysis<LoopInfoWrapperPass>();
    LoopInfo *LI = &wrapperPass.getLoopInfo();

    size_t outerLoopNum = 0;
    std::set<Loop*> loops;

    for (Loop *L: *LI) {
      outerLoopNum++;
      loops.insert(L);
      for (auto subloop: *L) {
        loops.insert(subloop);
      }
    }

    size_t loopNum = 0;
    size_t loopBBNum = 0;
    std::set<Loop::Edge> exitEdges;
    for (auto& L: loops) {
      loopNum++;
      loopBBNum += L->getBlocks().size();

      SmallVector<Loop::Edge, 8> exits;
      L->getExitEdges(exits);
      for (auto e: exits) {
        exitEdges.insert(e);
        //exitEdges.push_back(e);
      }
    }

    outs() << "number of loops: " << loopNum << "\n";
    outs() << "number of outermost loops: " << outerLoopNum << "\n";
    outs() << "number of exit edges: " << exitEdges.size() << "\n";
  }

  void doP3_2(Function& F) {
    LoopInfoWrapperPass &wrapperPass = getAnalysis<LoopInfoWrapperPass>();
    LoopInfo *LI = &wrapperPass.getLoopInfo();

    size_t outerLoopNum = 0;
    std::set<Loop*> loops;

    for (Loop *L: *LI) {
      outerLoopNum++;
      loops.insert(L);
      for (auto subloop: *L) {
        loops.insert(subloop);
      }
    }

    size_t cycleCount = 0;
    size_t singleEntryLoopCount = 0;
    size_t multiEntryLoopCount = 0;
    for (auto scci = scc_begin(&F), scce = scc_end(&F); scci != scce; ++scci) {
      const std::vector<BasicBlock *>& SCC = *scci;

      size_t entryCount = 0;
      bool isCycle = 0;
      for (auto& B: SCC) {
        for (auto PB: predecessors(B)) {
          // if PB not in this SCC
          if (std::find(SCC.begin(), SCC.end(), PB) == SCC.end()) {
            entryCount++;
          }
          else {
            isCycle = true;
          }
        }
      }

      if (isCycle) {
        cycleCount++;
        if (entryCount > 1) {
          multiEntryLoopCount++;
        }
      }
    }

    if (cycleCount != outerLoopNum) {
      outs() << "number of cycles: " << cycleCount << "\n";
      outs() << "number of outer loops: " << outerLoopNum << "\n";
    }

    outs() << "multi-entry outermost cycles: " <<  multiEntryLoopCount << "\n";
    outs() << "single-entry outermost cycles: " <<  cycleCount - multiEntryLoopCount << "\n";
  }

  void test(Function& F) {
    if (!F.getName().equals("LzmaEncode")) {
      return;
    }
    for (auto& B: F) {
      for (auto& I: B) {
        //I.hasName()
        outs() << I << "\n";
        outs() << I.getType()->isVoidTy() << "\n";
        outs() << I.getName() << "\n";
      }
    }
  }


  void doP3_4(Function& F) {
    auto LI = &getAnalysis<LoopInfoWrapperPass>().getLoopInfo();
    auto DT = &getAnalysis<DominatorTreeWrapperPass>().getDomTree();

    std::map<BasicBlock*, int> sccIDMap;
    int sccCount = 0;
    for (auto scci = scc_begin(&F), scce = scc_end(&F); scci != scce; ++scci) {
      const std::vector<BasicBlock *> &SCC = *scci;
      for (auto B: SCC) {
        sccIDMap[B] = sccCount;
      }
      sccCount++;
    }


    bool reachable = false;
    for (auto& A: F) {
      for (auto& B: F) {
        MyTimer timer;
        if (sccIDMap[&A] == sccIDMap[&B]) {
          reachable = true;
        }
        else {
          reachable = isPotentiallyReachable(&A, &B, DT, LI);
        }

        _totalTime += timer.elapsed();
        _totalTest++;

        // the average number of definitions that originate in block A and reach block B
        size_t defCount = 0;
        if (reachable) {
          for (auto& I: A) {
            if (!I.getType()->isVoidTy()) {
              defCount++;
            }
          }
        }
        _totalDefNum += defCount;
      }
    }
  }

  void reportP3_4() {
    outs() << format("average time for Reachable(A, B): %.6f", _totalTime/_totalTest) << "\n";
    outs() << format("the average number of reaching defs: %.6f", _totalDefNum/_totalTest) << "\n";
  }

  // Only need to add stuff here to make LoopInfoWrapperPass available for use
  void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.addRequired<LoopInfoWrapperPass>();
    AU.addRequired<DominatorTreeWrapperPass>();
    AU.addRequired<PostDominatorTreeWrapperPass>();
    AU.setPreservesAll();
  }
};
}

char HW1Part3::ID = 0;

static RegisterPass<HW1Part3> hw1part3_1pass("hw1-part3-1", "Pass for part 3.1 in HW1");





namespace {
struct HW1Part3_3 : public FunctionPass {
  static char ID; // Pass identification, replacement for typeid

  bool _verbose = true;


  HW1Part3_3() : FunctionPass(ID) {

  }

  bool runOnFunction(Function &F) override {
    if (_verbose) {
      outs() << F.getName() << "\n";
    }

    doP3_3(F);
    return false;
  }

  void doP3_3(Function& F) {
    auto PDT = &getAnalysis<PostDominatorTreeWrapperPass>().getPostDomTree();
    for (auto& Bj: F) {
      for (BasicBlock& Bi: F) {
        if (!dyn_cast<BranchInst>(Bi.getTerminator())) {
          continue;
        }

        // Bi ends with a branch

        if (PDT->dominates(&Bj, &Bi)) {
          continue;
        }

        // Bj does not dominate Bi
        for (BasicBlock* Bs: successors(&Bi)) {
          if (PDT->dominates(&Bj, Bs)) {
            outs() << "Block " << Bj.getName() << " is control dependent on " << Bs->getName() << "\n";
          }
        }
      }
    }
  }

  void test(Function& F) {
    if (!F.getName().equals("LzmaEncode")) {
      return;
    }
    for (auto& B: F) {
      for (auto& I: B) {
        //I.hasName()
        outs() << I << "\n";
        outs() << I.getType()->isVoidTy() << "\n";
        outs() << I.getName() << "\n";
      }
    }
  }

  // Only need to add stuff here to make LoopInfoWrapperPass available for use
  void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.addRequired<LoopInfoWrapperPass>();
    AU.addRequired<DominatorTreeWrapperPass>();
    AU.addRequired<PostDominatorTreeWrapperPass>();
    AU.setPreservesAll();
  }
};
}

char HW1Part3_3::ID = 0;

static RegisterPass<HW1Part3_3> hw1part3_3pass("hw1-part3-3", "Pass for part 3.3 in HW1");
