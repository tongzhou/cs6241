# Build
Place the directory in `llvm/lib/Transforms/` and add the directory into `llvm/lib/Transforms/CMakeLists.txt`.
Then do a standard LLVM build and this pass will be built together into a `llvm/build/lib/LLVM_CS6241_HW1.so`

# Run
Run part 2:
```bash
$ opt -load $(LLVM_BUILD_DIR)/lib/LLVM_CS6241_HW1.so -hw1-part2 xxx.ll
```

Run part 3, (3.1):
```bash
$ opt -load $(LLVM_BUILD_DIR)/lib/LLVM_CS6241_HW1.so -hw1-part3-1 xxx.ll
```

Run part 3, (3.3):
```bash
$ opt -load $(LLVM_BUILD_DIR)/lib/LLVM_CS6241_HW1.so -hw1-part3-3 xxx.ll
```

